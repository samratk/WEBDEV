1. img is the only self closing tag
2. Attributes
   1. class - applies special property to a group of elements.
   2. id - specifies a unique id to one element on the Page
   3. style - specifies certain visual styles.
   4. accesskey - accessibility.
   5. tabindex - order of tab. 
